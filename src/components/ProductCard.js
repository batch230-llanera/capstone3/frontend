import { Button, Card, Row, Container, Col} from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom"

export default function ProductCard({productProp}){

	const {_id, name, productImage, description, price, orderSlotsAvailable} = productProp;
	
	return(
		    <Card as={Col} className="p-3 m-3" style={{ width: '18rem', borderColor: "#D9A1A0"}}>
		      <Card.Img variant="top" src={productImage} style={{ height: '20rem'}}/>
		      <Card.Body style={{backgroundColor: "#EAE7FA"}}>
		        <Card.Title style={{fontFamily: "Papyrus, fantasy", fontSize: 24, color: "#A58CB3"}}>{name}</Card.Title>
		        <Card.Text style={{fontFamily: "Sacramento, cursive"}}>
		          {description}
		        </Card.Text>
		        <Card.Text style={{fontFamily: "Helvetica, sans-serif"}}>
		          Price: starts at {price} <br/>
		          Order Slots: {orderSlotsAvailable}
		        </Card.Text>
		        <Button style={{backgroundColor: "#A16AE8", borderColor: "#D9A1A0"}} as={Link} to={`/products/${_id}`}>Order</Button>
		      </Card.Body>
		    </Card>
		    	  
		  
	);
}


