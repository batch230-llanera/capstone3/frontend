import { Form, Button, Container, Col, Row } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext' 
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);


  
    function authenticate(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: { 'Content-Type' : 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log("Check accessToken");
            console.log(data.accessToken);

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token' , data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Leddie's Homemade Sweets!"
                })   
            }
            else{
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }


        }) 

        setEmail(''); 
        setPassword('');
    }


    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log("retrieveUserDetails: ")
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })

            
            localStorage.setItem("userId", data._id);
            localStorage.setItem("userRole", data.isAdmin);

            console.log("Login: ");
            console.log(user)

        })
    }


    useEffect(() => {

            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);


    return (
        (user.id !== null)
        ? 
        <Navigate to="/products" />
        :
        <Row className="justify-content-center" xs={1} md={2} lg={10} >
        <Col className="col">
        <Container fluid className="border p-5 m-5" style={{backgroundColor: "#EAE7FA"}} >
        <Form onSubmit = {(e) => authenticate(e)}>
        <h3 className="mt-3 mb-3" style={{fontFamily: "Papyrus, fantasy", fontSize: 24, color: "#A58CB3"}}> Login </h3>
            <Form.Group className="mb-3"controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required

                    required
                />
            </Form.Group>

             { isActive ?  
                <Button variant="primary" type="submit" id="submitBtn" style={{backgroundColor: "#A16AE8", borderColor: "#D9A1A0"}}>
                    Login
                </Button>
                : 
                <Button variant="success" type="submit" id="submitBtn" style={{backgroundColor: "#A16AE8", borderColor: "#D9A1A0"}} disabled>
                    Login
                </Button>
            }

        </Form>
        </Container>
        </Col>
        </Row>
        
    )
}

