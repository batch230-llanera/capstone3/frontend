import { useEffect, useState, useContext } from "react";

import { Button, Carousel, Container, Row, Col } from 'react-bootstrap';

import { Navigate } from "react-router-dom";

import GalleryCarousel from "../components/GalleryCarousel";
import UserContext from "../UserContext";

export default function Gallery() {

	const { user } = useContext(UserContext);
	const [gallery, setGallery] = useState([]);
	const [wedding, setWedding] = useState([]);
	const [message, setMessage] = useState([]);
	const [namePhoto, setNamePhoto] = useState([]);
	const [cupCake, setCupCake] = useState([]);
	const [chocolate, setChocolate] = useState([]);
	const [unique, setUnique] = useState([]);
	const [classic, setClassic] = useState([]);
	const [themed, setThemed] = useState([]);

	const getMessagePhotos = () => {

				fetch(`${process.env.REACT_APP_API_URL}/photos/messagePhotos`)
				.then(res => res.json())
				.then(data => {
					console.log(data);
					setMessage(data.map(message =>{
						return(							
						<Carousel.Item>
		  					<GalleryCarousel key={message._id} galleryProp={message} />
		  				</Carousel.Item>			  	
						);
					}));
				})
			}

	useEffect(() =>{
		getMessagePhotos()
	}, []);

	const getWeddingPhotos = () => {

				fetch(`${process.env.REACT_APP_API_URL}/photos/weddingPhotos`)
				.then(res => res.json())
				.then(data => {
					console.log(data);
					setWedding(data.map(wedding =>{
						return(							
						<Carousel.Item>
		  					<GalleryCarousel key={wedding._id} galleryProp={wedding} />
		  				</Carousel.Item>			  	
						);
					}));
				})
			}

	useEffect(() =>{
		getWeddingPhotos()
	}, []);

	const getNamePhotos = () => {

				fetch(`${process.env.REACT_APP_API_URL}/photos/namePhotos`)
				.then(res => res.json())
				.then(data => {
					console.log(data);
					setNamePhoto(data.map(namePhoto =>{
						return(							
						<Carousel.Item>
		  					<GalleryCarousel key={namePhoto._id} galleryProp={namePhoto} />
		  				</Carousel.Item>			  	
						);
					}));
				})
			}

	useEffect(() =>{
		getNamePhotos()
	}, []);

	const getCupCakePhotos = () => {

				fetch(`${process.env.REACT_APP_API_URL}/photos/cupCakePhotos`)
				.then(res => res.json())
				.then(data => {
					console.log(data);
					setCupCake(data.map(cupCake =>{
						return(							
						<Carousel.Item>
		  					<GalleryCarousel key={cupCake._id} galleryProp={cupCake} />
		  				</Carousel.Item>			  	
						);
					}));
				})
			}

	useEffect(() =>{
		getCupCakePhotos()
	}, []);

	const getChocolatePhotos = () => {

				fetch(`${process.env.REACT_APP_API_URL}/photos/chocolatePhotos`)
				.then(res => res.json())
				.then(data => {
					console.log(data);
					setChocolate(data.map(chocolate =>{
						return(							
						<Carousel.Item>
		  					<GalleryCarousel key={chocolate._id} galleryProp={chocolate} />
		  				</Carousel.Item>			  	
						);
					}));
				})
			}

	useEffect(() =>{
		getChocolatePhotos()
	}, []);

	const getUniquePhotos = () => {

				fetch(`${process.env.REACT_APP_API_URL}/photos/uniquePhotos`)
				.then(res => res.json())
				.then(data => {
					console.log(data);
					setUnique(data.map(unique =>{
						return(							
						<Carousel.Item>
		  					<GalleryCarousel key={unique._id} galleryProp={unique} />
		  				</Carousel.Item>			  	
						);
					}));
				})
			}

	useEffect(() =>{
		getUniquePhotos()
	}, []);

	const getClassicPhotos = () => {

				fetch(`${process.env.REACT_APP_API_URL}/photos/classicPhotos`)
				.then(res => res.json())
				.then(data => {
					console.log(data);
					setClassic(data.map(classic =>{
						return(							
						<Carousel.Item>
		  					<GalleryCarousel key={classic._id} galleryProp={classic} />
		  				</Carousel.Item>			  	
						);
					}));
				})
			}

	useEffect(() =>{
		getClassicPhotos()
	}, []);


	const getThemedPhotos = () => {

				fetch(`${process.env.REACT_APP_API_URL}/photos/themedPhotos`)
				.then(res => res.json())
				.then(data => {
					console.log(data);
					setThemed(data.map(themed =>{
						return(							
						<Carousel.Item>
		  					<GalleryCarousel key={themed._id} galleryProp={themed} />
		  				</Carousel.Item>			  	
						);
					}));
				})
			}

	useEffect(() =>{
		getThemedPhotos()
	}, []);



	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" />
		:
		<Container fluid>
			<h1 className="p-3 text-center" style={{fontFamily: "Lucida Handwriting, cursive", color: "#D9A1A0"}}>Our Baked Goodies</h1>
			<h4 className="text-center" style={{fontFamily: "Lucida Handwriting, cursive", color: "#A58CB3", fontSize: 16}}>"Happiness is Homemade"</h4>
			<Row className="justify-content-center" xs={1} md={4} lg={8}>
				
			<Carousel as={Col} className="p-2" style={{ width: '20rem' }} fade>	
					{message}
	  		</Carousel>
	  		<Carousel as={Col} className="p-2" style={{ width: '20rem' }} fade>	
					{wedding}
	  		</Carousel>
	  		<Carousel as={Col} className="p-2" style={{ width: '20rem' }} fade>	
					{namePhoto}
	  		</Carousel>
	  		<Carousel as={Col} className="p-2" style={{ width: '20rem' }} fade>	
					{cupCake}
	  		</Carousel>
	  		<Carousel as={Col} className="p-2" style={{ width: '20rem' }} fade>	
					{chocolate}
	  		</Carousel>
	  		<Carousel as={Col} className="p-2" style={{ width: '20rem' }} fade>	
					{unique}
	  		</Carousel>
	  		<Carousel as={Col} className="p-2" style={{ width: '20rem' }} fade>	
					{classic}
	  		</Carousel>
	  		<Carousel as={Col} className="p-2" style={{ width: '20rem' }} fade>	
					{themed}
	  		</Carousel>
	  		
	  		
	  		
	  		</Row>
  		</Container>
  	)
}


